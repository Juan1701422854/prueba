package main

import (
	"context"

	"net/http"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"github.com/go-chi/chi"
	"google.golang.org/grpc"
)

type Comprador struct {
	LQS1 string `json:"Id"`
	LQS2 string `json:"Name`
	LQS3 int
}

var (
	p []Comprador
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func main() {
	r := chi.NewRouter()
	conn, _ := grpc.Dial("localhost:9080", grpc.WithInsecure())
	defer conn.Close()
	dg := dgo.NewDgraphClient(api.NewDgraphClient(conn))
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Bienvenido a la pagina inicial"))
	})
	r.Get("/{Date}", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		date := chi.URLParam(r, "Date")
		resp1, _ := dg.NewTxn().QueryWithVars(context.Background(), ` query all($date: string)
			{
				transfers(func: eq(Date, $date)){
					Transferid
					Date
			  		F as BuyerId
					Ip
					Device					
				}
			
				buyers(func: eq(Id, val(F))){
					Id
			  		Name
			  		Age
				}
		  	}
			`, map[string]string{"$date": date})
		w.Write(resp1.GetJson())
	})
	r.Route("/buyers", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			enableCors(&w)
			txn := dg.NewTxn()
			const q1 = `
			{
				allBuyers(func: has(Id)){
					Id
					Name	
					Age
				}
			}
			`
			resp1, _ := txn.Query(context.Background(), q1)
			w.Write(resp1.GetJson())
		})
		r.Get("/{id}", func(w http.ResponseWriter, r *http.Request) {
			enableCors(&w)
			id := chi.URLParam(r, "id")
			resp1, _ := dg.NewTxn().QueryWithVars(context.Background(), ` query all($id: string)
			{
				buyers(func: eq(Id, $id)) {
					buyer as Id
			  		Name
			  		Age
				}

				transfers(func: eq(BuyerId, val(buyer))) {
					t as Transferid
				}

				product(func: eq(Transferid, val(t))) {
					ProductsId
				}
		  	}
			`, map[string]string{"$id": id})
			w.Write(resp1.GetJson())
		})
	})
	r.Route("/ips", func(r chi.Router) {
		r.Get("/{id}", func(w http.ResponseWriter, r *http.Request) {
			enableCors(&w)
			id := chi.URLParam(r, "id")
			resp1, _ := dg.NewTxn().QueryWithVars(context.Background(), ` query all($id: string)
			{
				buyer(func: eq(BuyerId, $id)) {
					ip as Ip
				}
				ips(func: eq(Ip, val(ip))){
					buyer as BuyerId
				}
				otherBuyers(func: eq(Id, val(buyer))){
					Id
					Name
					Age
				}
		  	}
			`, map[string]string{"$id": id})
			w.Write(resp1.GetJson())
		})
	})
	http.ListenAndServe(":3000", r)
}
