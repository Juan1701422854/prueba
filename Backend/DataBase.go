package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

//Crear la estructura de tipo comprador
type Comprador struct {
	Id   string `json:"Id"`
	Name string `json:"Name"`
	Age  int    `json:"Age"`
}

//Crear la estructura de tipo producto
type Producto struct {
	Productid    string
	Productname  string
	Productprice int
}

//Crear la estructura de tipo transferencia
type Transferencia struct {
	Transferid string
	BuyerId    string
	Ip         string
	Device     string
	ProductsId []string
	Date       string
}

func EncontrarComprador(id string) Comprador {
	var c Comprador
	for i := 0; i < len(p); i++ {
		if p[i].Id == id {
			c = p[i]
			break
		}
	}
	return c
}

//Metodo que me crea una lista de compradores a partir de la api -> json
func LlenarComprador() {
	respuesta, _ := http.Get("https://kqxty15mpg.execute-api.us-east-1.amazonaws.com/buyers")
	datos, _ := ioutil.ReadAll(respuesta.Body)
	json.Unmarshal([]byte(datos), &p)
}

//Metodo que me crea una lista de productos a partir de la api -> csv separado por '
func LlenarProducto() {
	texto, _ := http.Get("https://kqxty15mpg.execute-api.us-east-1.amazonaws.com/products")
	datos, _ := ioutil.ReadAll(texto.Body)
	cadena := strings.Split(string(datos), "\n")
	var product Producto
	var cadena2 []string
	for i := 0; i < len(cadena)-1; i++ {
		counter := 0
		for j := 0; j < len(cadena[i]); j++ {
			if string(cadena[i][j]) == "'" {
				counter++
			}
		}
		if counter > 2 {
			aux := strings.Split(cadena[i], "'")
			text := ""
			for k := 0; k < len(aux); k++ {
				if k == 0 {
					cadena2 = append(cadena2, aux[k])
				} else if k == len(aux)-1 {
					cadena2 = append(cadena2, text)
					cadena2 = append(cadena2, aux[k])
				} else {
					if k == len(aux)-2 {
						text = text + aux[k]
					} else {
						text = text + aux[k] + "'"
					}

				}
			}
		} else {
			aux := strings.Split(cadena[i], "'")
			for k := 0; k < len(aux); k++ {
				cadena2 = append(cadena2, aux[k])
			}
		}
	}
	for i := 0; i < len(cadena2); i = i + 3 {
		product.Productid = cadena2[i]
		product.Productname = cadena2[i+1]
		product.Productprice, _ = strconv.Atoi(cadena2[i+2])
		productsList = append(productsList, product)
	}
}

//Metodo que me crea una lista de transferencias a partir de la api -> sin formato
func LlenarTransferencia() {
	texto, _ := http.Get("https://kqxty15mpg.execute-api.us-east-1.amazonaws.com/transactions")
	datos, _ := ioutil.ReadAll(texto.Body)
	datos2 := bytes.Split(datos, []byte(string(0)))
	counter := 1
	var T2 Transferencia
	for i := 0; i < len(datos2); i++ {
		switch counter {
		case 1:
			T2.Transferid = string(datos2[i])
			counter++
		case 2:
			T2.BuyerId = string(datos2[i])
			counter++
		case 3:
			T2.Ip = string(datos2[i])
			counter++
		case 4:
			T2.Device = string(datos2[i])
			counter++
		case 5:
			p := addToList(string(datos2[i]))
			T2.ProductsId = p
			counter++
		default:
			vr := time.Unix(time.Now().Unix(), 0)
			date := rand.Intn(vr.Day()) + 1
			dateS := "0" + strconv.Itoa(date)
			dateF := "2020-08-" + dateS
			T2.Date = dateF
			T = append(T, T2)
			counter = 1
		}
	}
}

//Funcion que me devuelve true o false si encuentra alguno de los caracteres
func Splits(r rune) bool {
	return r == '(' || r == ',' || r == ')'
}

//Agrega a la lista -> Metodo usado por LlenarTrasnferencia
func addToList(s string) []string {
	p := strings.FieldsFunc(s, Splits)
	return p
}

//Variables globales
var (
	p            []Comprador
	productsList []Producto
	T            []Transferencia
	dg           *dgo.Dgraph
)

func main() {
	conn, err := grpc.Dial("localhost:9080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	dg := dgo.NewDgraphClient(api.NewDgraphClient(conn))
	dg.Alter(context.Background(), &api.Operation{DropOp: api.Operation_ALL})
	LlenarComprador()
	for i := 0; i < len(p); i++ {
		txn1 := dg.NewTxn()
		defer txn1.Discard(context.Background())
		buyer := Comprador{
			Id:   p[i].Id,
			Name: p[i].Name,
			Age:  p[i].Age,
		}
		pb, _ := json.Marshal(buyer)
		mu := &api.Mutation{
			SetJson: pb,
		}
		txn1.Mutate(context.Background(), mu)
		txn1.Commit(context.Background())
	}
	LlenarProducto()
	for i := 0; i < len(productsList); i++ {
		txn2 := dg.NewTxn()
		defer txn2.Discard(context.Background())
		product := Producto{
			Productid:    productsList[i].Productid,
			Productname:  productsList[i].Productname,
			Productprice: productsList[i].Productprice,
		}
		pb, _ := json.Marshal(product)
		mu := &api.Mutation{
			SetJson: pb,
		}
		txn2.Mutate(context.Background(), mu)
		txn2.Commit(context.Background())
	}
	LlenarTransferencia()
	for i := 0; i < len(T); i++ {
		txn3 := dg.NewTxn()
		defer txn3.Discard(context.Background())
		transfer := Transferencia{
			Transferid: T[i].Transferid,
			BuyerId:    T[i].BuyerId,
			Ip:         T[i].Ip,
			Device:     T[i].Device,
			ProductsId: T[i].ProductsId,
			Date:       T[i].Date,
		}
		pb, _ := json.Marshal(transfer)
		mu := &api.Mutation{
			SetJson: pb,
		}
		fmt.Println(i)
		txn3.Mutate(context.Background(), mu)
		txn3.Commit(context.Background())
	}
}
